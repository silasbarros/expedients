class Expedient < ApplicationRecord
  belongs_to :exp_type
  belongs_to :destination  #, optional: true
  belongs_to :responsible
end
