json.extract! expedient, :id, :date_exp, :topic, :spu, :exp_type_id, :destination_id, :responsible_id, :created_at, :updated_at
json.url expedient_url(expedient, format: :json)
